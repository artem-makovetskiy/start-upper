<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
//Route::get('login')->name('login')->uses('Auth\LoginController@showLoginForm')->middleware('guest');
//Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login')->middleware('guest');
//Route::post('logout')->name('logout')->uses('Auth\LoginController@logout');

// Dashboard
Route::get('/')->name('dashboard')->uses('DashboardController');

// 500 error
//Route::get('500', function () {
//    // Force debug mode for this endpoint in the demo environment
//    if (App::environment('demo')) {
//        Config::set('app.debug', true);
//    }
//
//    echo $fail;
//});
